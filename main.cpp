#include "tritset.h"
#include <fstream>
using namespace std;

int main()
{
	TritSet A(100);
  TritSet B(100);
  A[0]=False;
  A[14] = A[0];
  A[13]=True;
  A[118]=False;
  B[0]=True;
  A|=B;
	ofstream fout("out.txt");
	fout << A;
	return 0;
}
