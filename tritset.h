#pragma once
#include <iostream>
#include <stdlib.h>
#include <limits.h>
#include <cstddef>
#include <cstring>
#include <algorithm>
#include <map>
#include <exception>

typedef unsigned int uint;
const int block_size = sizeof(uint) * CHAR_BIT; //32 bites
const int number_of_trits = block_size/2;
enum Trit {Unknown,False,True};

class TritSet
{
private:
    uint *ptr;
    uint size;
public:
    class TritHolder
    {
    private:
        TritSet *trit_set;
        uint block_index;
        uint trit_index;
		    Trit trit_value;
    public:
		    TritHolder(TritSet*, uint, uint);
        TritHolder& operator= (const Trit&);
		TritHolder& operator= (TritHolder&) = delete;
		    operator Trit () const;
    };
    TritSet();
    TritSet(uint); //конструктор с параметрами
    TritSet(const TritSet &);
    TritSet(std::initializer_list<Trit>);
    TritSet& operator= (const TritSet& other);
    ~TritSet(); //деструктор
    uint get_size() const;
    uint capacity() const;
	  uint cardinality(const Trit& value) const;
  	std::map<Trit, int> cardinality() const;
  	void shrink();
  	void trim(uint index);
  	int length() const;
    void set_trit(uint trit_index, const Trit& value);
  	Trit get_trit(uint index) const;
  	TritSet operator& (const Trit&) const;
  	TritSet operator& (TritSet& other) const;
    TritSet& operator&= (const Trit&);
    TritSet& operator&= (TritSet& other);
  	TritSet operator| (const Trit&) const;
  	TritSet operator| (TritSet& other) const;
    TritSet& operator|= (const Trit&);
    TritSet& operator|= (TritSet& other);
    TritSet operator~ (void) const;
  	bool operator== (const TritSet& other) const;
  	bool operator!= (const TritSet& other) const;
    void show_data() const;
    TritHolder operator[] (uint index);
	  Trit operator[] (uint index) const;
};
Trit operator& (const Trit& value1, const Trit& value2);
Trit operator| (const Trit& value1, const Trit& value2);
Trit operator~ (const Trit& value);
Trit& operator&= (Trit& value1, const Trit& value2);
Trit& operator|= (Trit& value1, const Trit& value2);
std::ostream& operator<< (std::ostream&, const Trit&);
std::ostream& operator<< (std::ostream&, const TritSet&);
