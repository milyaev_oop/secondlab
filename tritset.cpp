#include "tritset.h"


TritSet::TritSet()
{
    size = 0;
    ptr = nullptr;
}

TritSet::TritSet(uint set_size)
{
    size = (set_size > 0 ? ((set_size - 1)/(block_size / 2) + 1) : 0);
    ptr = new uint [size]();
}

TritSet::TritSet(const TritSet &tritsetToCopy) : size(tritsetToCopy.size)
{
    ptr = new uint [size]();
	memcpy(ptr, tritsetToCopy.ptr, sizeof(uint)*size);
}

TritSet::TritSet(std::initializer_list<Trit> list)
{
	uint set_size = list.size();
	size = (set_size > 0 ? ((set_size - 1) / (block_size / 2) + 1) : 0);
	ptr = new uint[size]();
	for (uint i = 0;i < set_size;i++)
	{
		(*this).set_trit(i,list.begin()[i]);
	}
}

TritSet::~TritSet()
{
    delete[] ptr;
    ptr = nullptr;
}

void TritSet::show_data() const
{
	std::cout << "Data:\n";
	for (int i = 0; i < size; i++) {
		std::cout << i * number_of_trits << ":\t";
		for (int j = 0; j < number_of_trits; j++)
			std::cout << (*this)[i * number_of_trits + j];
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

void TritSet::set_trit(uint index, const Trit& value)
{
  uint trit_index = index % number_of_trits;
  uint block_index = index / number_of_trits;
  size_t bit_index = trit_index * 2;
  uint shift = block_size - bit_index % block_size - 1;
  if (block_index >= size && value!=Unknown)
  {
    uint *dest_ptr = new uint[block_index + 1]();
    memcpy (dest_ptr, ptr, sizeof(uint) * size);
    delete [] ptr;
    ptr = dest_ptr;
    size = block_index + 1;
  }
  if (value == True)
  {
      ptr[block_index] |= 1 << shift;
      ptr[block_index] &= ~(1 << (shift - 1));
  }
  else if (value == False)
  {
      ptr[block_index] &= ~(1 << shift);
      ptr[block_index] |= 1 << (shift - 1);
  }
  else
  {
      ptr[block_index] &= ~(1 << shift);
      ptr[block_index] &= ~(1 << (shift - 1));
  }
}

int TritSet::length() const
{
	int number_of_last_trit = -1;
	for (int i = size * number_of_trits - 1; i >= 0; i--)
	{
			if ((*this)[i] == True || (*this)[i] == False)
			{
				number_of_last_trit = i;
				return number_of_last_trit + 1;
			}
	}
	return number_of_last_trit + 1;
}

void TritSet::shrink()
{
	if (length() == 0)
	{
		delete[] ptr;
		ptr = nullptr;
		size = 0;
	}
	else
	{
		uint trit_index = (length() - 1) % number_of_trits;
		uint block_index = (length() - 1) / number_of_trits;
		size_t bit_index = trit_index * 2;
		uint shift = block_size - bit_index % block_size - 1;
		if (block_index < size - 1)
		{
			uint *dest_ptr = new uint[block_index + 1]();
			memcpy(dest_ptr, ptr, sizeof(uint) * (block_index + 1));
			delete[] ptr;
			ptr = dest_ptr;
			size = block_index + 1;
		}
	}
}

void TritSet::trim(uint index)
{
	if (index == 0)
	{
		delete[] ptr;
		ptr = nullptr;
		size = 0;
	}
	else
	{
		uint trit_index = index % number_of_trits;
		uint block_index = index / number_of_trits;
		for (int i = trit_index;i < number_of_trits;i++)
		{
			(*this)[i] = Unknown;
		}
		if (block_index < size - 1)
		{
			uint *dest_ptr = new uint[block_index + 1]();
			memcpy(dest_ptr, ptr, sizeof(uint) * (block_index + 1));
			delete[] ptr;
			ptr = dest_ptr;
			size = block_index + 1;
		}
	}
}

std::ostream& operator<< (std::ostream& out, const Trit& value)
{
	switch (value) {
		case True: out << "T "; break;
		case False: out << "F "; break;
		case Unknown: out << "U "; break;
    default: throw std::range_error("bad_trit\n");
	}
	return out;
}

std::ostream& operator<< (std::ostream& out, const TritSet& t)
{
  for (int i = 0; i < t.capacity(); i++)
		out << t[i];
  return out;
}

//T 10
//F 01
//U 00

TritSet::TritHolder::TritHolder(TritSet *t, uint block, uint trit)
	: trit_set(t), block_index(block), trit_index(trit), trit_value(t->get_trit(block*number_of_trits + trit))
{

}

TritSet::TritHolder TritSet::operator[] (uint index)
{
TritHolder t(this, index/number_of_trits, index % number_of_trits);
return t;
}

Trit TritSet::operator[] (uint index) const
{
	return get_trit(index);
}

TritSet::TritHolder& TritSet::TritHolder::operator= (const Trit& value)
{
  trit_set->set_trit(block_index * number_of_trits + trit_index,value);
  trit_value = value;
  return *this;
}

/*TritSet::TritHolder& TritSet::TritHolder::operator= (TritHolder& trit_holder)
{
	std::cout << "TritHolderOperator=TritHolder" << std::endl;
	trit_set->set_trit(block_index * number_of_trits + trit_index, trit_holder.trit_value);
	trit_value = trit_holder.trit_value;
	return *this;
}*/

TritSet& TritSet::operator= (const TritSet& other)
{
	uint *dest_ptr = new uint[other.size]();
	memcpy(dest_ptr, other.ptr, sizeof(uint) * (other.size));
	delete[] ptr;
	ptr = dest_ptr;
	size = other.size;
	return (*this);
}

Trit TritSet::get_trit (uint index) const
{
  if (size * number_of_trits - 1 < index || size == 0)
  {
    return Unknown;
  }
  size_t bit_index = index % number_of_trits * 2;
  int shift = block_size - (bit_index) - 2;
  int value = (ptr[index / number_of_trits] >> shift) & 3;
  switch(value)
  {
	  case 2: return True; break;
    case 1: return False; break;
    case 0: return Unknown; break;
    default: throw std::range_error("bad_trit\n");
  }
}

Trit operator& (const Trit& value1, const Trit& value2)
{
	if (value1==False || value2==False)
		return False;
	if (value1==Unknown || value2==Unknown)
		return Unknown;
	return True;
}

Trit operator| (const Trit& value1, const Trit& value2)
{
	if (value1==True || value2==True)
		return True;
	if (value1==Unknown || value2==Unknown)
		return Unknown;
	return False;
}

Trit operator~ (const Trit& value)
{
	if (value==False)
		return True;
	if (value==True)
		return False;
	return Unknown;
}

Trit& operator&= (Trit& value1, const Trit& value2)
{
	if (value1==False || value2==False)
		return value1 = False;
	if (value1==Unknown || value2==Unknown)
		return value1 = Unknown;
	return value1 = True;
}

Trit& operator|= (Trit& value1, const Trit& value2)
{
	if (value1==True || value2==True)
		return value1 = True;
	if (value1==Unknown || value2==Unknown)
		return value1 = Unknown;
	return value1 = False;
}

TritSet TritSet::operator& (const Trit& value) const
{
	TritSet result(number_of_trits * size);
	uint all_trits = number_of_trits * size;
	for (uint i = 0; i < all_trits;i++)
		result[i] = (*this)[i] & value;
	return result;
}

TritSet TritSet::operator& (TritSet& other) const
{
	TritSet result(number_of_trits * std::max(size, other.size));
	size_t all_trits = std::max(size, other.size) * number_of_trits;
	for (size_t i = 0; i < all_trits; i++)
		result[i] = (*this)[i] & other[i];
	return result;
}

TritSet& TritSet::operator&= (const Trit& value)
{
  (*this) = (*this) & value;
  return (*this);
}

TritSet& TritSet::operator&= (TritSet& other)
{
  (*this) = (*this) & other;
  return (*this);
}

TritSet TritSet::operator| (const Trit& value) const
{
	TritSet result(number_of_trits * size);
	uint all_trits = number_of_trits * size;
	for (uint i = 0; i < all_trits;i++)
		result[i] = (*this)[i] | value;
	return result;
}

TritSet TritSet::operator| (TritSet& other) const
{
	TritSet result(number_of_trits * std::max(size, other.size));
	size_t all_trits = std::max(size, other.size) * number_of_trits;
	for (size_t i = 0; i < all_trits; i++)
		result[i] = (*this)[i] | other[i];
	return result;
}

TritSet& TritSet::operator|= (const Trit& value)
{
  (*this) = (*this) | value;
  return (*this);
}

TritSet& TritSet::operator|= (TritSet& other)
{
  (*this) = (*this) | other;
  return (*this);
}

TritSet TritSet::operator~ () const
{
	TritSet result(number_of_trits * size);
	size_t all_trits = size * number_of_trits;
	for (size_t i = 0; i < all_trits; i++)
		result[i] = ~((*this)[i]);
	return result;
}

bool TritSet::operator== (const TritSet& other) const
{
	if (size != other.size || length()!=other.length())
		return false;
	else
		for (int i = 0;i < size;i++)
		{
			if (ptr[i] != other.ptr[i])
				return false;
		}
	return true;
}

bool TritSet::operator!= (const TritSet& other) const
{
	return !((*this)==other);
}

TritSet::TritHolder::operator Trit () const
{
	return trit_value;
}

uint TritSet::capacity() const
{
	return number_of_trits * size;
}

uint TritSet::cardinality(const Trit& value) const
{
	uint count = 0;
	for (int i = 0;i < (*this).length();i++)
	{
		if ((*this)[i] == value)
			count++;
	}
	return count;
}

std::map<Trit, int> TritSet::cardinality() const
{
	uint count = 0;
	std::map<Trit,int> Cardinality;
	for (int i = 0;i < (*this).length();i++)
	{
		Cardinality[(*this)[i]]++;
	}
	return Cardinality;
}
uint TritSet::get_size() const
{
  return size;
}
