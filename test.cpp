#include <gtest/gtest.h>
#include "tritset.h"

using namespace std;
TEST(get_size,test)
{
  TritSet A;
  TritSet B(100);
  TritSet C(1827);
  ASSERT_EQ(A.get_size(),0);
  ASSERT_EQ(B.get_size(),7);
  ASSERT_EQ(C.get_size(),115);
}
TEST(capacity,test)
{
  TritSet A;
  TritSet B(100);
  TritSet C(1827);
  ASSERT_EQ(A.capacity(),0);
  ASSERT_EQ(B.capacity(),112);
  ASSERT_EQ(C.capacity(),1840);
}
TEST(cardinality,test1)
{
  TritSet A(16);
  A[0]=False;
  A[13]=True;
  A[14]=True;
  ASSERT_EQ(A.cardinality(False),1);
  ASSERT_EQ(A.cardinality(True),2);
  ASSERT_EQ(A.cardinality(Unknown),12);
}
TEST(cardinality,test2)
{
  TritSet A(16);
  A[0]=False;
  A[13]=True;
  A[14]=True;
  ASSERT_EQ(A.cardinality()[False],1);
  ASSERT_EQ(A.cardinality()[True],2);
  ASSERT_EQ(A.cardinality()[Unknown],12);
}
TEST(length,test)
{
  TritSet A(100);
  A[0]=False;
  A[13]=True;
  A[14]=True;
  A[53]=True;
  ASSERT_EQ(A.length(),54);
}
TEST(shrink,test)
{
  TritSet A(100);
  A[0]=False;
  A[13]=True;
  A[14]=True;
  A[53]=True;
  A.shrink();
  ASSERT_EQ(A.get_size(),4);
}
TEST(trim,test)
{
  TritSet A(100);
  A[0]=False;
  A[13]=True;
  A[14]=True;
  A[53]=True;
  A.trim(7);
  ASSERT_EQ(A.get_size(),1);
}
TEST(set_trit,test)
{
  TritSet A(100);
  A[0]=False;
  A[13]=True;
  A[14]=True;
  A[53]=True;
  ASSERT_EQ(A[12],Unknown);
  A.set_trit(12,False);
  ASSERT_EQ(A[12],False);
  ASSERT_EQ(A[0],False);
  A.set_trit(0,True);
  ASSERT_EQ(A[0],True);
  ASSERT_EQ(A[53],True);
  A.set_trit(53,Unknown);
  ASSERT_EQ(A[53],Unknown);
}
TEST(get_trit,test)
{
  TritSet A(100);
  A[0]=False;
  A[13]=True;
  A[14]=True;
  A[53]=True;
  ASSERT_EQ(A.get_trit(12),Unknown);
  ASSERT_EQ(A.get_trit(0),False);
  ASSERT_EQ(A.get_trit(53),True);
  ASSERT_EQ(A.get_trit(200),Unknown);
}
TEST(TritSetoperators,test1)
{
  TritSet A(100);
  TritSet B(200);
  A[0]=False;
  A[13]=True;
  A[14]=False;
  A[53]=True;
  B[0]=True;
  B[15]=True;
  B[59]=False;
  TritSet C;
  C[0]=False;
  C[13]=Unknown;
  C[14]=False;
  C[15]=Unknown;
  C[53]=Unknown;
  C[59]=False;
  TritSet D = A&B;
  D.shrink();
  ASSERT_EQ(C,D);
}
TEST(TritSetoperators,test2)
{
  TritSet A(100);
  TritSet B(200);
  A[0]=False;
  A[13]=True;
  A[14]=False;
  A[53]=True;
  B[0]=True;
  B[15]=True;
  B[59]=False;
  TritSet C;
  C[0]=True;
  C[13]=True;
  C[14]=Unknown;
  C[15]=True;
  C[53]=True;
  C[59]=Unknown;
  TritSet D = A|B;
  D.shrink();
  ASSERT_EQ(C,D);
}
TEST(TritSetoperators,test3)
{
  TritSet A(100);
  A[0]=False;
  A[13]=True;
  TritSet C;
  C[0]=False;
  C[13]=True;
  TritSet D = A&True;
  D.shrink();
  ASSERT_EQ(C,D);
}
TEST(TritSetoperators,test4)
{
  TritSet A(100);
  A[0]=False;
  A[13]=True;
  TritSet C;
  for (int i=0;i<112;i++)
    C[i]=False;
  TritSet D = A&False;
  D.shrink();
  ASSERT_EQ(C,D);
}
TEST(TritSetoperators,test5)
{
  TritSet A(100);
  A[0]=False;
  A[13]=True;
  TritSet C;
  C[0]=False;
  C[13]=Unknown;
  TritSet D = A&Unknown;
  D.shrink();
  ASSERT_EQ(C,D);
}
TEST(TritSetoperators,test6)
{
  TritSet A(100);
  A[0]=False;
  A[13]=True;
  TritSet C;
  for (int i=0;i<112;i++)
    C[i]=True;
  TritSet D = A|True;
  D.shrink();
  ASSERT_EQ(C,D);
}
TEST(TritSetoperators,test7)
{
  TritSet A(100);
  A[0]=False;
  A[13]=True;
  TritSet C;
  C[0]=False;
  C[13]=True;
  TritSet D = A|False;
  D.shrink();
  ASSERT_EQ(C,D);
}
TEST(TritSetoperators,test8)
{
  TritSet A(100);
  A[0]=False;
  A[13]=True;
  TritSet C;
  C[13]=True;
  TritSet D = A|Unknown;
  D.shrink();
  ASSERT_EQ(C,D);
}
TEST(TritSetoperators,test9)
{
  TritSet A(100);
  A[0]=False;
  A[13]=True;
  A[118]=True;
  TritSet C = A;
  ASSERT_EQ(C,A);
}
TEST(TritSetoperators,test10)
{
  TritSet A(100);
  TritSet B(100);
  A[0]=False;
  A[13]=True;
  A[118]=True;
  B[0]=True;
  B[13]=False;
  B[118]=False;
  TritSet C = ~A;
  ASSERT_EQ(C,B);
}
TEST(TritSetoperators,test11)
{
  TritSet A(100);
  TritSet B(100);
  A[0]=False;
  A[13]=True;
  A[118]=True;
  B[0]=True;
  B[13]=False;
  B[118]=False;
  int k;
  if (A!=B)
    k=1;
  ASSERT_EQ(k,1);
}
TEST(TritSetoperators,test12)
{
  TritSet A(100);
  TritSet B(100);
  A[0]=False;
  A[13]=True;
  A[118]=True;
  B[0]=False;
  B[13]=True;
  B[118]=True;
  int k;
  if (A==B)
    k=1;
  ASSERT_EQ(k,1);
}
TEST(TritSetoperators,test13)
{
  TritSet A(100);
  TritSet B(100);
  A[0]=False;
  A[13]=True;
  A[118]=False;
  B[0]=True;
  A&=B;
  TritSet C;
  C[0]=False;
  C[118]=False;
  A.shrink();
  C.shrink();
  ASSERT_EQ(A,C);
}
TEST(TritSetoperators,test14)
{
  TritSet A(100);
  TritSet B(100);
  A[0]=False;
  A[13]=True;
  A[118]=False;
  B[0]=True;
  A|=B;
  TritSet C;
  C[0]=True;
  C[13]=True;
  A.shrink();
  C.shrink();
  ASSERT_EQ(A,C);
}
TEST(Tritoperators,test1)
{
  Trit A=True;
  A&=False;
  ASSERT_EQ(A,False);
  Trit B=False;
  A&=B;
  ASSERT_EQ(A,False);
  A=True;
  A&=True;
  ASSERT_EQ(A,True);
  B=True;
  A&=B;
  ASSERT_EQ(A,True);
  A=True;
  A&=Unknown;
  ASSERT_EQ(A,Unknown);
  B=Unknown;
  A&=B;
  ASSERT_EQ(A,Unknown);
}
TEST(Tritoperators,test2)
{
  Trit A=False;
  A&=False;
  ASSERT_EQ(A,False);
  Trit B=False;
  A&=B;
  ASSERT_EQ(A,False);
  A=False;
  A&=True;
  ASSERT_EQ(A,False);
  B=True;
  A&=B;
  ASSERT_EQ(A,False);
  A=False;
  A&=Unknown;
  ASSERT_EQ(A,False);
  B=Unknown;
  A&=B;
  ASSERT_EQ(A,False);
}
TEST(Tritoperators,test3)
{
  Trit A=Unknown;
  A&=False;
  ASSERT_EQ(A,False);
  Trit B=False;
  A&=B;
  ASSERT_EQ(A,False);
  A=Unknown;
  A&=True;
  ASSERT_EQ(A,Unknown);
  B=True;
  A&=B;
  ASSERT_EQ(A,Unknown);
  A=Unknown;
  A&=Unknown;
  ASSERT_EQ(A,Unknown);
  B=Unknown;
  A&=B;
  ASSERT_EQ(A,Unknown);
}
TEST(Tritoperators,test4)
{
  Trit A=True;
  A|=False;
  ASSERT_EQ(A,True);
  Trit B=False;
  A|=B;
  ASSERT_EQ(A,True);
  A=True;
  A|=True;
  ASSERT_EQ(A,True);
  B=True;
  A|=B;
  ASSERT_EQ(A,True);
  A=True;
  A|=Unknown;
  ASSERT_EQ(A,True);
  B=Unknown;
  A|=B;
  ASSERT_EQ(A,True);
}
TEST(Tritoperators,test5)
{
  Trit A=False;
  A|=False;
  ASSERT_EQ(A,False);
  Trit B=False;
  A|=B;
  ASSERT_EQ(A,False);
  A=False;
  A|=True;
  ASSERT_EQ(A,True);
  B=True;
  A|=B;
  ASSERT_EQ(A,True);
  A=False;
  A|=Unknown;
  ASSERT_EQ(A,Unknown);
  B=Unknown;
  A|=B;
  ASSERT_EQ(A,Unknown);
}
TEST(Tritoperators,test6)
{
  Trit A=Unknown;
  A|=False;
  ASSERT_EQ(A,Unknown);
  Trit B=False;
  A|=B;
  ASSERT_EQ(A,Unknown);
  A=Unknown;
  A|=True;
  ASSERT_EQ(A,True);
  B=True;
  A|=B;
  ASSERT_EQ(A,True);
  A=Unknown;
  A|=Unknown;
  ASSERT_EQ(A,Unknown);
  B=Unknown;
  A|=B;
  ASSERT_EQ(A,Unknown);
}
